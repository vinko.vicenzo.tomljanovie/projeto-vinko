import { extendTheme } from "@chakra-ui/react";
import fonts from "./fonts";
import sizes from "./sizes";
import colors from "./colors"

export const theme = extendTheme({
  styles: {
    global: {
      "*[data-focus]": {
        boxShadow: "none !important",
      },
    },
  },
  sizes,
  colors: colors,
  space: sizes,
  components: {
    Button: {
      baseStyle: {
        _focus: { boxShadow: "none !important" },
        _hover: { opacity: "0.8 !important" },
        transition: "all 0.3s !important",
      },
    },
    Link: { baseStyle: { _focus: { boxShadow: "none !important" } } },
    Checkbox: {
      baseStyle: {
        control: {
          borderRadius: "base",
        },
      },
    },
  },
  ...fonts,
});
