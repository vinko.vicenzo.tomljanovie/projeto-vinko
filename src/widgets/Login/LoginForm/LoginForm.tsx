import React from "react";
import LoginFormInterface from './LoginForm.types';
import * as Yup from "yup";
import { Stack, Text, Box, Input, chakra, Center } from "@chakra-ui/react";

import { Inputs } from "../../../components/Inputs/Index";
import { SvgIcons } from "../../../components/SvgIcons/SvgIcons";
import { Form, Formik } from "formik";
import { isEmpty, size } from "lodash";
import { SubmitButton } from "../../../components/Buttons/SubmitButton/SubmitButton";
import { useNavigate } from "react-router-dom";
import { PopUp } from "../../../components/PopUp/PopUp";

export const LoginForm: React.FC<LoginFormInterface> = (props) => {
    const {initialValues, onSubmit = () => null } = props;

    const loginSchema = Yup.object().shape({
        user: Yup.string().required("Informação obrigatória"),
        pass: Yup.string().required("Senha obrigatório"),
    });

    return(
        <Formik
      initialValues={initialValues}
      validationSchema={loginSchema}
      onSubmit={(values, actions) => onSubmit(values, actions)}
      validateOnBlur={true}
      validateOnChange={true}
    >
      {({ errors, values, handleChange, touched, setFieldTouched }) => {
        return (
          <Form>
            <Stack
              display="flex"
              alignItems="center"
              h="100vh"
              justifyContent="center"
              spacing={4}
            >
              <Box
                w="60vh"
                h="60vh"
                alignItems="center"
                display="flex"
                flexDir="column"
                justifyContent="center"
                bgColor="blackAlpha.900"
                borderRadius="md"
              >
        
                <Text fontSize="20" color="whitesmoke" fontWeight="700">
                  Olá, faca seu{" "}
                  <chakra.span color="orange.500" textDecoration="underline">
                    Login
                  </chakra.span>
                </Text>
                <Stack spacing={5} mt={8}>
                  <Inputs
                    variant="default"
                    errors={errors?.user}
                    id="user"
                    value={values?.user}
                    onChange={handleChange}
                    isInvalid={!isEmpty(errors?.user) && touched?.user}
                    onBlur={() => setFieldTouched("user")}
                    h="10"
                    w="full"
                    label="User"
                    placeholder="Usuário"
                    inputLeftElement={true}
                    leftChildren={
                      <SvgIcons
                        bg="orange.500"
                        h={4}
                        src="icon_user.svg"
                        w={4}
                      />
                    }
                  />
                  <Inputs
                    variant="password"
                    h="10"
                    errors={errors?.pass}
                    id="pass"
                    value={values?.pass}
                    onChange={handleChange}
                    isInvalid={!isEmpty(errors?.pass) && touched?.pass}
                    onBlur={() => setFieldTouched("pass")}
                    label="Senha"
                    w="full"
                    placeholder="Digite sua senha"
                    inputLeftElement={true}
                    leftChildren={
                      <SvgIcons
                        bg="orange.500"
                        h={4}
                        src="icon_pass.svg"
                        w={4}
                      />
                    }
                  />

                  <SubmitButton
                    bgColor="orange.500"
                    rounded="md"
                    color="whitesmoke"
                    type="submit"
                  >
                    ENTRAR
                  </SubmitButton>
                </Stack>
              </Box>
            </Stack>
          </Form>
        );
      }}
    </Formik>
    );
}