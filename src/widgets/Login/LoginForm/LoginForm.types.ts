import { FormikHelpers } from "formik";

type loginInitialValues = {
    user: string;
    pass: string;
}

interface LoginFormInterface {
    initialValues: loginInitialValues;
    onSubmit: (value: loginInitialValues, actions: FormikHelpers<loginInitialValues>) => void;
}

export default LoginFormInterface;