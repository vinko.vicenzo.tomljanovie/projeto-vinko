import { FormikHelpers } from "formik";

type LoginProfileValues = {
  user: string;
  pass: string;
};
