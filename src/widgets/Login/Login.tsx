import React, { useState, useMemo, useCallback } from "react";
import * as Yup from "yup";
import { useLogin } from "../../actions/hooks";
import { useNavigate } from "react-router-dom";
import { PopUp } from "../../components/PopUp/PopUp";
import { LoginForm } from "./LoginForm/LoginForm";
import { Stack } from "@chakra-ui/react";

export const Login: React.FC = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [erroModal, setErroModal] = useState(false);
  const navigate = useNavigate();

  const onSubmit = (user: string, pass: string) => {
    if (useLogin(user, pass)) {
      navigate("/home");
      setIsLoggedIn(true);
    } else {
      setErroModal(true);
    }
  };

  return (
    <Stack>
      <LoginForm initialValues={{ user: '', pass: ''}} onSubmit={(values) => onSubmit(values.user, values.pass)}/>
      <PopUp 
        isOpen={erroModal} 
        message="O usuário/senha que você digitou está errado, por favor digite novamente!" 
        onClose={() => { 
          setErroModal(false);
          window.location.reload();
        }}
        size='xs'
        title="Usuário ou Senha incorretos!"
        />
    </Stack>
  );
};
