import { Stack, Text } from "@chakra-ui/react";
import { FirstBanner, SecondBanner } from "../../components";
import React from "react";

export const Home: React.FC = (props) => {
  return (
    <>
      <FirstBanner />
      <SecondBanner />
    </>
  );
};
