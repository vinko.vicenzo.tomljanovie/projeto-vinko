import React from "react";
import { InputDefault } from "./InputDefault/InputDefault";
import { InputPass } from "./InputPass/InputPassword";
import { InputInterface } from "./Inputs.types";

export const Inputs: React.FC<InputInterface> = (props) => {
  const { variant } = props;

  if (variant == "password") return <InputPass {...props} />;
  return <InputDefault {...props} />;
};
