type VariantType =
  | "default"
  | "password"
  | "select"
  | "menu"
  | "date"
  | "credit-card"
  | "email"
  | "input-date";

interface InputDefaultInterface {
  label?: string;
  w: string;
  h: string;
  errors?: string;
  isInvalid?: boolean;
  value: string;
  id: string;
  onBlur?: (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => void;
  onChange?: (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => void;
  placeholder?: string;
  inputLeftElement: boolean;
  leftChildren: React.ReactNode;
}

interface InputInterface extends InputDefaultInterface {
  variant: VariantType;
}

export type { InputDefaultInterface, InputInterface };
