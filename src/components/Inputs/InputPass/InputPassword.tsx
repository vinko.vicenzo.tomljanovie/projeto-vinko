import {
  Input,
  InputGroup,
  InputLeftElement,
  FormControl,
  FormLabel,
  FormErrorMessage,
  InputRightElement,
  Button,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { SvgIcons } from "../../SvgIcons/SvgIcons";
import { InputDefaultInterface } from "../Inputs.types";

export const InputPass: React.FC<InputDefaultInterface> = (props) => {
  const {
    w,
    h,
    placeholder,
    label,
    isInvalid,
    errors,
    inputLeftElement,
    onChange = () => null,
    onBlur = () => null,
    leftChildren,
    value,
    id,
  } = props;

  const [show, setShow] = useState(false);
  const handleClick = () => {
    setShow(!show);
  };

  return (
    <FormControl isInvalid={isInvalid}>
      <FormLabel fontSize="xs" fontWeight={700} color="whitesmoke">
        {label}:
      </FormLabel>
      <InputGroup>
        {inputLeftElement && (
          <InputLeftElement>{leftChildren}</InputLeftElement>
        )}
        <Input
          h={h}
          w={w}
          color="whitesmoke"
          placeholder={placeholder}
          _placeholder={{ color: "whitesmoke" }}
          _active={{}}
          _hover={{}}
          _focus={{}}
          onChange={onChange}
          value={value}
          onBlur={onBlur}
          id={id}
          autoComplete="on"
          type={!show ? "password" : "text"}
        />

        <InputRightElement>
          <SvgIcons
            bg="orange.500"
            h={5}
            src={show ? "/icon_show_pass.svg" : "/icon_no_show_pass.svg"}
            w={5}
            role="button"
            onClick={handleClick}
          />
        </InputRightElement>
      </InputGroup>
      <FormErrorMessage p="0" mt="1" color="orange.500">
        {errors}
      </FormErrorMessage>
    </FormControl>
  );
};
