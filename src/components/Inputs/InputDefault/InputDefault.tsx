import {
  Stack,
  Box,
  Text,
  Input,
  InputGroup,
  InputLeftElement,
  FormControl,
  FormErrorMessage,
  FormLabel,
} from "@chakra-ui/react";
import React from "react";
import { SvgIcons } from "../../SvgIcons/SvgIcons";
import { InputDefaultInterface } from "../Inputs.types";

export const InputDefault: React.FC<InputDefaultInterface> = (props) => {
  const {
    w,
    h,
    placeholder,
    label,
    errors,
    inputLeftElement,
    leftChildren,
    isInvalid,
    onChange = () => null,
    onBlur = () => null,
    value,
    id,
  } = props;

  return (
    <FormControl isInvalid={isInvalid}>
      <FormLabel fontSize="xs" fontWeight={700} color="whitesmoke">
        {label}:
      </FormLabel>
      <InputGroup>
        {inputLeftElement && (
          <InputLeftElement>{leftChildren}</InputLeftElement>
        )}
        <Input
          h={h}
          w={w}
          onChange={onChange}
          onBlur={onBlur}
          value={value}
          id={id}
          color="whitesmoke"
          placeholder={placeholder}
          _placeholder={{ color: "whitesmoke" }}
          _active={{}}
          _hover={{}}
          _focus={{}}
        />
      </InputGroup>
      <FormErrorMessage p="0" mt="1" color="orange.500">
        {errors}
      </FormErrorMessage>
    </FormControl>
  );
};
