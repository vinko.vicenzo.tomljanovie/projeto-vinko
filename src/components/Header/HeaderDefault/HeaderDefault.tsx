import { HStack, Box, Text, Flex, Link } from "@chakra-ui/react";
import React from "react";
import { HeaderDefaultProps } from "../Header.types";

export const HeaderDefault: React.FC<HeaderDefaultProps> = (props) => {
  const { headerLinks } = props;

  return (
    <HStack display="flex" w="full" flexDir="row" alignItems="center" h="12vh" >
      <Box px="15%">
        <Text fontSize="22px">Vinko</Text>
      </Box>
      <Flex
        w="70%"
        justifyContent="space-evenly"
        fontSize="18px"
        fontWeight="800"
      >
        {headerLinks.map((headerLink, index) => {
          return (
            <Link textDecoration="none" href={headerLink.link} key={index} lineHeight='1'>
              {headerLink.text}
            </Link>
          );
        })}
      </Flex>
    </HStack>
  );
};
