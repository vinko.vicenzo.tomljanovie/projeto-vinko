import React from "react";
import { HeaderProps } from "./Header.types"
import { HeaderDefault } from "./HeaderDefault/HeaderDefault";

export const Header: React.FC<HeaderProps> = (props) => {
    const { type } = props;

    if (type === 'default') 
    return <HeaderDefault {...props}/>
    else return <></>
}