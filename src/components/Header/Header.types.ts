type VariantType =
  | "default";

type HeaderLinks = {
  text: string;
  link: string;
}

interface HeaderDefaultProps {
  headerLinks: HeaderLinks[];
}
  
interface HeaderProps extends HeaderDefaultProps {
  type: VariantType;
}

export { HeaderDefaultProps, HeaderProps}