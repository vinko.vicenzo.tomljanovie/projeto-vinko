import { AriaRole } from "react";

interface SvgIconsInterface {
  src: string;
  h: number;
  w: number;
  bg: string;
  role?: AriaRole;
  onClick?: () => void;
}

export type { SvgIconsInterface };
