import { MotionBox } from "../../motion";
import React from "react";
import { SvgIconsInterface } from "./SvgIcons.types";

export const SvgIcons: React.FC<SvgIconsInterface> = (props) => {
  const { src, h, w, onClick, bg, role } = props;
  return (
    <MotionBox
      {...props}
      onClick={onClick}
      role={role}
      bg={bg}
      sx={{
        w: w,
        h: h,
        mask: `url(../../../public/${src}) no-repeat center/contain`,
      }}
    />
  );
};
