export interface PopUpinterface {
  size: string;
  title: string;
  isOpen: boolean;
  message: string;
  onClose: () => void;
}
