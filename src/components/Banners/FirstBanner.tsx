import { Box, Stack, Image, Flex, Text } from "@chakra-ui/react";
import React from "react";
import { DefaultButton } from "../Buttons";

export const FirstBanner: React.FC = (props) => {
  return (
    <Stack display="flex" h="100">
      <Box
        display="flex"
        w="60%"
        bgColor="blue.600"
        h="full"
        alignItems="center"
        pl="15%"
      >
        <Box maxW="50vh" color="whitesmoke">
          <Text fontSize="md">Hello</Text>
          <Text fontWeight="900" fontSize="4xl" lineHeight='1'>
            I'am, Vinko
          </Text>
          <Text fontSize="2xl">Lorem ipsum dolor sit amet</Text>
          <Text fontSize="xs" mt="1vh">
            Donec dui ante, lacinia in magna vitae, tincidunt volutpat ligula.
            Nulla elementum imperdiet ornare. Maecenas maximus metus vitae metus
            vehicula egestas
          </Text>
          <DefaultButton
            w="20"
            mt='4'
            h="8"
            text="Hire Me"
            bg="whitesmoke"
            color="black"
            fontSize="xs"
            onClick={() => console.log("Me contrate")}
          />
        </Box>
        <Flex
          w="52vh"
          h="50vh"
          alignItems="center"
          justifyContent="center"
          borderRadius="100%"
          position="absolute"
          border="8px solid white"
          right="27%"
          p="2vh"
        >
          <Image
            src="../../../perfil_vinko.png"
            w="full"
            h="full"
            borderRadius="100%"
          />
        </Flex>
      </Box>
      
    </Stack>
  );
};
