import { Box, Flex, Stack, Text } from "@chakra-ui/react";
import React from "react";
import { DefaultButton } from "../Buttons";

export const SecondBanner: React.FC = (props) => {
  return (
    <Stack display="flex" h="110" pt="15">
      <Box display="flex" w="47%" h="full" alignItems="center" pl="15%">
        <Box>
          <Text fontSize="3xl" fontWeight="600" color="gray.600">
            About Me
          </Text>
          <Text w="full" fontSize="sm" mt="9">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla erat
            velit, gravida eu mattis sed, semper elementum libero. Fusce
            hendrerit pretium erat sed vulputate. Proin tristique ex vitae est
            malesuada, ut interdum nunc.
          </Text>
          <Text fontSize="sm" mt="4">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla erat
            velit, gravida eu mattis sed, semper elementum libero. Fusce
            hendrerit pretium erat sed vulputate. Proin tristique ex vitae est
            malesuada, ut interdum nunc tempor.
          </Text>
          <DefaultButton
            w="24"
            h="8"
            text="Download CV"
            bg="blue.600"
            color="whitesmoke"
            mt="14"
            fontSize="xs"
            onClick={() => console.log("Baixar CV")}
          />
        </Box>
      </Box>
    </Stack>
  );
};
