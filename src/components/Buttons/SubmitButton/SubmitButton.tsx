import React from "react";
import { Button, ButtonProps } from "@chakra-ui/react";

export const SubmitButton: React.FC<ButtonProps> = (props) => {
  const { children } = props;
  return (
    <Button _hover={{ opacity: 0.9 }} {...props}>
      {children}
    </Button>
  );
};
