import React from "react";
import { DefaultButtonProps } from "../Buttons.types";
import { Text, Box } from "@chakra-ui/react";

export const DefaultButton: React.FC<DefaultButtonProps> = (props) => {
  const { bg, color, h, onClick = () => null, text, w, fontSize, mt } = props;

  return (
    <Box
      bg={bg}
      justifyContent="center"
      alignItems="center"
      h={h}
      w={w}
      borderRadius="3"
      onClick={onClick}
      display="flex"
      mt={mt}
    >
      <Text color={color} fontSize={fontSize}>
        {text}
      </Text>
    </Box>
  );
};
