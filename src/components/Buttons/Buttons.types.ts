export interface DefaultButtonProps {
  w?: string;
  h?: string;
  text?: string;
  bg?: string;
  color?: string;
  onClick?: () => void;
  mt?: string;
  fontSize?: string;
}
