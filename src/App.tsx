import React from "react";
import { ChakraProvider, Stack } from "@chakra-ui/react";
import { Login, Home } from "./widgets";
import { Header } from "./components";
import { theme } from "../theme/theme";
import { useNavigate, Route, Routes } from "react-router-dom";

export const App = () => {
  const headerLinks = [
    {
      text: "Home",
      link: "/",
    },
    {
      text: "About Me",
      link: "/",
    },
    {
      text: "Projeto 1",
      link: "/",
    },
  ];

  return (
    <ChakraProvider theme={theme}>
      <Stack bg='bgNagem'>
      <Header type="default" headerLinks={headerLinks}/>       
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </Stack>
    </ChakraProvider>
  );
};

export default App;
