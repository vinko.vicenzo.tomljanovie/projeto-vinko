import { motion } from 'framer-motion';
import { Box, BoxProps, Image, ImageProps } from '@chakra-ui/react';
const MotionBox = motion<Omit<BoxProps, 'transition'>>(Box);
const MotionImage = motion<Omit<ImageProps, 'transition'>>(Image);

export { MotionBox, MotionImage };
